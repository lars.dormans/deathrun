package com.thecreepercow.deathrun;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.*;
import com.thecreepercow.deathrun.Setup.Traps.TrapList;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.jetbrains.annotations.NotNull;

@CommandAlias("DTR|DR")
public class Commands extends BaseCommand {
    public Commands() {
    }
    @Dependency
    private Plugin plugin;

    @Subcommand("lobby|l")
    public class LobbyCommands extends BaseCommand {

    }

    @Subcommand("setup|s")
    @CommandPermission("dtr.setup")
    @Conditions("setup")
    public class SetupCommands extends BaseCommand {

        @Subcommand("stick")
        public void setupStickCommand(@NotNull Player player){
            Deathrun.getInstance().getSetup().getSetupManager().giveTool(player);
        }

        @Subcommand("trap")
        @CommandCompletion("@traptype")
        public void createTrapCommand(String trapType, String trapName, Player player){
            TrapList type = null;
            try{
                type = TrapList.valueOf(trapType);
            }catch (IllegalArgumentException e){
                player.sendMessage(Lang.getString("SetupInvalidType"));
            }
            if (type!=null) {
                Deathrun.getInstance().getSetup().getSetupManager().createTrap(TrapList.valueOf(trapType), trapName, player);
            }
        }
        @Subcommand("interactable")
        public void createInteractableCommand(int type, Player player, @Optional String extra){
            if (type>2||type<1){
                player.sendMessage(Lang.getString("SetupIntInvalid"));
            }else{
                Deathrun.getInstance().getSetup().getSetupManager().createInteractable(type,player, extra);
            }
        }

        @Subcommand("save")
        public void saveMap(Player player){
            String worldName = player.getWorld().getName();
            Deathrun.getInstance().getSetup().getMapManager().saveMap(worldName,Deathrun.getInstance().getSetup().getMapManager().getMap(worldName));
        }
        @Subcommand("setup")
        public void setupMap(Player player, String worldname){
            Deathrun.getInstance().getSetup().getMapManager().setupMap(worldname,player);
        }

        @Subcommand("setspawns")
        public void setSpawns(Player player){
            Deathrun.getInstance().getSetup().getSetupManager().setSpawns(player);
        }

        @Subcommand("setdeathspawns")
        public void setDeathSapwns(Player player){
            Deathrun.getInstance().getSetup().getSetupManager().setDeathSpawns(player);
        }

        @Subcommand("setname")
        public void setName(String name){
            Deathrun.getInstance().getSetup().getMapManager().getActiveMap().setName(name);
        }
    }

    @Subcommand("vote")
    public void vote(Player player, int i){

    }

}
