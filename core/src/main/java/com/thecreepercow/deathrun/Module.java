package com.thecreepercow.deathrun;

import org.bukkit.event.Listener;

public abstract class Module implements Listener {
    public Module() {
        Deathrun.getInstance().registerListener(this);
    }

    public abstract void initialize();

    //Possibility to add more abstract or set method if required
}
