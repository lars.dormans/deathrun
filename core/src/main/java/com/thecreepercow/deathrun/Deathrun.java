package com.thecreepercow.deathrun;

import co.aikar.commands.BukkitCommandManager;
import co.aikar.commands.ConditionFailedException;
import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;
import com.thecreepercow.deathrun.Game.GameModule;
import com.thecreepercow.deathrun.Game.GameStatus;
import com.thecreepercow.deathrun.Lobby.LobbyMode;
import com.thecreepercow.deathrun.Lobby.Shop.ItemTypes;
import com.thecreepercow.deathrun.Lobby.Signs.UpdateInfo;
import com.thecreepercow.deathrun.Lobby.User;
import com.thecreepercow.deathrun.Setup.SetupModule;
import com.thecreepercow.deathrun.Setup.Traps.TrapList;
import com.thecreepercow.deathrun.Storage.MySQLDatabase;
import com.thecreepercow.deathrun.Util.StaticItemStack;
import org.bukkit.Bukkit;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.messaging.PluginMessageListener;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.*;

public final class Deathrun extends JavaPlugin implements PluginMessageListener {
    private Lang language;
    private File messageFile;
    private static Deathrun instance;
    private boolean bungeeMode = false;
    private boolean lobbyMode = false;
    private boolean setupMode = false;
    private LobbyMode lobby;
    private SetupModule setup;
    private GameModule game;
    private String serverName = null;
    @Nullable
    private MySQLDatabase database;
    private BukkitCommandManager cmdManager;

    @Override
    public void onEnable() {
        // Plugin startup logic
        setup();
        instance = this;
        initialize();
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }

    private void setup() {
        saveDefaultConfig();
        messageFile = new File(getDataFolder(), "message.yml");
        if (!messageFile.exists()) {
            messageFile.getParentFile().mkdirs();
            saveResource("message.yml", false);
        }
        FileConfiguration messages = new YamlConfiguration();
        try {
            messages.load(messageFile);
        } catch (@NotNull IOException | InvalidConfigurationException e) {
            //TODO Shutdown
            e.printStackTrace();
        }
        language = new Lang(messages);
        try {
            database = new MySQLDatabase(getConfig().getString("SQL.host"), getConfig().getString("SQL.username"), getConfig().getString("SQL.password"), getConfig().getString("SQL.database"));
        } catch (SQLException e) {
            //TODO Shutdown
            e.printStackTrace();
        }
        Bukkit.getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
        Bukkit.getMessenger().registerIncomingPluginChannel(this, "BungeeCord", this);
        StaticItemStack.initialize();

    }

    private void initialize() {
        cmdManager = new BukkitCommandManager(this);
        cmdManager.registerDependency(BukkitCommandManager.class, cmdManager);
        cmdManager.getCommandContexts().registerIssuerAwareContext(User.class, User.getResolver());
        cmdManager.getCommandCompletions().registerStaticCompletion("itemtype", () -> {
            List<String> list = new ArrayList<>();
            for (ItemTypes type : ItemTypes.values()) {
                list.add(type.getCategoryName());
            }
            return list;
        });
        cmdManager.getCommandCompletions().registerStaticCompletion("traptype", () -> {
            Collection<String> names = new ArrayList<>();
            for (TrapList trapList : TrapList.values()) {
                names.add(trapList.name());
            }
            return names;
        });
        cmdManager.getCommandConditions().addCondition("setup", context -> {
            if (!isSetupMode()) {
                throw new ConditionFailedException("Setup mode is not enabled in config");
            }
        });
        cmdManager.getCommandCompletions().registerCompletion("itemsplayer", context -> lobby.getShopModule().getItemNames(context.getPlayer()));
        cmdManager.getCommandCompletions().registerStaticCompletion("items",lobby.getShopModule().getItemNames(null));
        //TODO Add more CMD manager stuff here if needed
        cmdManager.registerCommand(new Commands());
        bungeeMode = getConfig().getBoolean("BungeeMode");
        lobbyMode = getConfig().getBoolean("lobbyMode");
        setupMode = getConfig().getBoolean("setupMode");
        lobby = new LobbyMode();
        setup = new SetupModule();
        game = new GameModule();
        lobby.initialize();
        if (!lobbyMode){
            setup.initialize();
            game.initialize();
        }
    }

    public static Deathrun getInstance() {
        return instance;
    }

    public void onPluginMessageReceived(@NotNull String s, @NotNull Player player, @NotNull byte[] bytes) {
        if (!s.equals("BungeeCord")) return;
        ByteArrayDataInput input = ByteStreams.newDataInput(bytes);
        String subChannel = input.readUTF();
        if (subChannel.equals("updateinfo") && isLobbyMode()) {
            short len = input.readShort();
            byte[] msgbytes = new byte[len];
            input.readFully(msgbytes);
            DataInputStream msgin = new DataInputStream(new ByteArrayInputStream(msgbytes));
            UpdateInfo info = null;
            try {
                info = UpdateInfo.fromString(msgin.readUTF());
            } catch (IOException e) {
                e.printStackTrace();
            }
            lobby.getSignManager().updateSigns(info);
        }else if(subChannel.equals("GetServer")){
            serverName = input.readUTF();
            UpdateInfo info = new UpdateInfo(serverName, game.getPreGameManager().getPlayerCount(), "PreGame", GameStatus.PreGame);
            UpdateInfo.send(info, (Player) game.getPreGameManager().getPlayers().keySet().toArray()[0]);
        }
    }

    public void registerListener(@NotNull Listener listener) {
        getServer().getPluginManager().registerEvents(listener, this);
    }

    public boolean isLobbyMode() {
        return lobbyMode;
    }

    public boolean isSetupMode() {
        return setupMode;
    }

    public boolean isBungeeMode() {
        return bungeeMode;
    }

    public LobbyMode getLobby() {
        return lobby;
    }

    @Nullable
    public MySQLDatabase getDatabase() {
        return database;
    }

    public SetupModule getSetup() {
        return setup;
    }

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public GameModule getGame() {
        return game;
    }
}
