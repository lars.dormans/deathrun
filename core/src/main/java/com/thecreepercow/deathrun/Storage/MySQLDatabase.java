package com.thecreepercow.deathrun.Storage;

import com.thecreepercow.deathrun.Deathrun;
import org.apache.ibatis.jdbc.ScriptRunner;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;

public class MySQLDatabase {
    @NotNull
    HashMap<String, PreparedStatement> savedStatements = new HashMap<>();
    private Connection conn;
    private ScriptRunner sr;

    public MySQLDatabase(String address, String user, String password, String database) throws SQLException {
        String url = "jdbc:mysql://" + address + "/" + database;
        conn = DriverManager.getConnection(url, user, password);
        sr = new ScriptRunner(conn);
    }

    private void setup() {
        InputStream file = Deathrun.getInstance().getResource("deathrun.sql");
        Reader reader = new InputStreamReader(file);
        sr.runScript(reader);
    }

    @Nullable
    public PreparedStatement prepare(String sql) {
        try {
            return conn.prepareCall(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    public void saveQuery(String identifier, PreparedStatement stmt) {
        savedStatements.put(identifier, stmt);
    }

    public PreparedStatement retrieveQuery(String identifier) {
        return savedStatements.get(identifier);
    }


}
