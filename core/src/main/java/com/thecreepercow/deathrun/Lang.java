package com.thecreepercow.deathrun;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class Lang {
    @Nullable
    static FileConfiguration messages = null;

    public Lang(FileConfiguration messages) {
        Lang.messages = messages;
    }

    @NotNull
    public static String getString(@NotNull String message) {
        if (messages == null) {
            return "Lang not initialized (How tf did this happen?)";
        }
        String msg = messages.getString(message);
        if (msg != null) {
            return ChatColor.translateAlternateColorCodes('&', msg);
        } else {
            return "Invalid message code";
        }
    }

}
