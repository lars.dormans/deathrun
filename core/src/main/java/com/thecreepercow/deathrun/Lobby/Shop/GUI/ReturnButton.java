package com.thecreepercow.deathrun.Lobby.Shop.GUI;

import com.thecreepercow.deathrun.Deathrun;
import com.thecreepercow.deathrun.Lang;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.jetbrains.annotations.NotNull;

public class ReturnButton extends BaseButton {
    BaseInventory previousGUI;

    public ReturnButton(BaseInventory previousGUI) {
        super(null);
        this.previousGUI = previousGUI;
        ItemStack button = new ItemStack(Material.REDSTONE_BLOCK);
        ItemMeta meta = button.getItemMeta();
        meta.setDisplayName(Lang.getString("GUIReturn"));
        button.setItemMeta(meta);
        setButton(button);
    }

    public ReturnButton(ItemStack button, BaseInventory previousGUI) {
        super(button);
        this.previousGUI = previousGUI;
    }

    @Override
    public void onClick(@NotNull InventoryClickEvent e) {
        Player player = (Player) e.getWhoClicked();
        player.closeInventory();
        Deathrun.getInstance().getLobby().getShopModule().openInventory(player, previousGUI);
    }
}
