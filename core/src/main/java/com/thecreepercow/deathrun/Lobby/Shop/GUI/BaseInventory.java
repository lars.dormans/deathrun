package com.thecreepercow.deathrun.Lobby.Shop.GUI;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

public abstract class BaseInventory {
    private int inventorySize = 0;
    private String inventoryTitle = "";
    @NotNull
    private Map<Integer, BaseButton> items = new HashMap<>();

    public BaseInventory(int inventorySize, String inventoryTitle) {
        this.inventorySize = inventorySize;
        this.inventoryTitle = inventoryTitle;
    }

    public void setBackground(ItemStack background) {
        for (int i = 0; i < inventorySize; i++) {
            BackgroundButton button = new BackgroundButton(background);
            items.put(i, button);
        }
    }

    public boolean setItem(int location, BaseButton button) {
        boolean isOccupied = items.containsKey(location);
        if (!isOccupied || allowOverride()) {
            if (location <= inventorySize - 1 && location >= 0) {
                items.put(location, button);
                return true;
            }
        }
        return false;
    }

    public BaseButton getButton(int position) {
        return items.get(position);
    }

    public abstract Inventory create(Player player);

    @NotNull
    Inventory finalCreation() {
        Inventory invent = Bukkit.createInventory(null, inventorySize, inventoryTitle);
        items.forEach((integer, baseButton) -> {
            invent.setItem(integer, baseButton.getButton());
        });
        return invent;
    }

    abstract boolean allowOverride();

    public abstract void onInventoryClose(InventoryCloseEvent event);
}
