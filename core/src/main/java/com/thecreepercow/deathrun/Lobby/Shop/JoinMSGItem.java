package com.thecreepercow.deathrun.Lobby.Shop;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;

public class JoinMSGItem implements IShopItem {
    String name;
    int price;
    String description;
    String message;

    public JoinMSGItem(String name, int price, String description, String message) {
        this.name = name;
        this.price = price;
        this.description = description;
        this.message = message;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getPrice() {
        return price;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @NotNull
    @Override
    public ItemTypes getType() {
        return ItemTypes.JoinMSG;
    }

    @NotNull
    @Override
    public ItemStack getItemStack() {
        ItemStack item = new ItemStack(Material.PAPER);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(ChatColor.LIGHT_PURPLE + name);
        meta.setLore(Arrays.asList(ChatColor.LIGHT_PURPLE + description, message));
        item.setItemMeta(meta);
        return item;
    }

    public String getJoinMessage() {
        return message;
    }

    @NotNull
    public String makeJoinMessage(@NotNull Player player) {
        return message.replace("%player%", player.getName());
    }
}
