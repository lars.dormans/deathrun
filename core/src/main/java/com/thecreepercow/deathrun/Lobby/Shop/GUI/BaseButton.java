package com.thecreepercow.deathrun.Lobby.Shop.GUI;

import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

public abstract class BaseButton {
    private ItemStack button;

    public BaseButton(ItemStack button) {
        this.button = button;
    }

    public abstract void onClick(InventoryClickEvent e);

    public ItemStack getButton() {
        return button;
    }

    public void setButton(ItemStack button) {
        this.button = button;
    }
}
