package com.thecreepercow.deathrun.Lobby.Signs;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import com.thecreepercow.deathrun.Deathrun;
import com.thecreepercow.deathrun.Game.GameStatus;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class UpdateInfo {
    /* This object is given to active game signs to allow them to update the sign.
     this object is created on player join or status change */
    // Server is only used in bungee mode to update the correct sign
    @Nullable
    private String server = null;
    private int players;
    private String map;
    private GameStatus status;

    public UpdateInfo(int players, String map, GameStatus status) {
        this.players = players;
        this.map = map;
        this.status = status;
    }

    public UpdateInfo(String server, int players, String map, GameStatus status) {
        this.server = server;
        this.players = players;
        this.map = map;
        this.status = status;
    }

    public int getPlayers() {
        return players;
    }

    public void setPlayers(int players) {
        this.players = players;
    }

    public String getMap() {
        return map;
    }

    public void setMap(String map) {
        this.map = map;
    }

    public GameStatus getStatus() {
        return status;
    }

    public void setStatus(GameStatus status) {
        this.status = status;
    }

    @Nullable
    public String getServer() {
        return server;
    }

    @NotNull
    @Override
    public String toString() {
        if (server != null) {
            return server + "|" + players + "|" + map + "|" + status;
        } else {
            return players + "|" + map + "|" + status;
        }
    }

    @NotNull
    public static UpdateInfo fromString(@NotNull String string) {
        String[] input = string.split("|");
        if (input.length == 4) {
            return new UpdateInfo(input[0], Integer.parseInt(input[1]), input[2], GameStatus.valueOf(input[3]));
        } else {
            return new UpdateInfo(Integer.parseInt(input[0]), input[1], GameStatus.valueOf(input[2]));
        }
    }

    public static void send(@NotNull UpdateInfo info, @NotNull Player player) {
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF("Forward");
        out.writeUTF("ALL");
        out.writeUTF("updateinfo");
        ByteArrayOutputStream msgbytes = new ByteArrayOutputStream();
        DataOutputStream msgout = new DataOutputStream(msgbytes);
        try {
            msgout.writeUTF(info.toString());

        } catch (IOException e) {
            e.printStackTrace();
        }
        out.writeShort(msgbytes.toByteArray().length);
        out.write(msgbytes.toByteArray());
        player.sendPluginMessage(Deathrun.getInstance(), "BungeeCord", out.toByteArray());
    }
    public static void getServerName(Player player){
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF("GetServer");
        player.sendPluginMessage(Deathrun.getInstance(), "BungeeCord", out.toByteArray());
    }
}
