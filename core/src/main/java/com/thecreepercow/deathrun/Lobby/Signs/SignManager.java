package com.thecreepercow.deathrun.Lobby.Signs;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.thecreepercow.deathrun.Deathrun;
import com.thecreepercow.deathrun.Lang;
import com.thecreepercow.deathrun.Lobby.Shop.ItemTypes;
import com.thecreepercow.deathrun.Module;
import org.bukkit.Location;
import org.bukkit.block.Sign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.*;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Objects;

public class SignManager extends Module {
    @Nullable
    private HashMap<Location, ISign> signs = new HashMap<>();
    @NotNull
    private Gson gson = new GsonBuilder().excludeFieldsWithModifiers(Modifier.TRANSIENT, Modifier.FINAL, Modifier.STATIC).enableComplexMapKeySerialization().serializeNulls().create();
    private static final Type type = new TypeToken<HashMap<Location, ISign>>() {
    }.getType();

    @Override
    public void initialize() {
        File file = new File(Deathrun.getInstance().getDataFolder() + "signs.json");
        if (file.exists()) {
            signs = loadSigns(file);
        }
    }

    @EventHandler
    public void onSignChange(@NotNull SignChangeEvent event) {
        if (Objects.requireNonNull(event.getLine(0)).equalsIgnoreCase("[dtr]") && event.getPlayer().hasPermission("dtr.lobby.setup")) {
            switch (Objects.requireNonNull(event.getLine(1))) {
                case "bungee":
                    String serverName = event.getLine(2);
                    BungeeGameSign sign = new BungeeGameSign(serverName);
                    sign.onPlace((Sign) event.getBlock().getState());
                    signs.put(event.getBlock().getLocation(), sign);
                    break;
                case "shop":
                    String category = event.getLine(2);
                    ItemTypes type = null;
                    try {
                        type = ItemTypes.valueOf(category);
                    } catch (IllegalArgumentException e) {
                        event.getPlayer().sendMessage(Lang.getString("CategoryNotExists"));
                    }
                    ShopSign shopsign;
                    if (type != null) {
                        shopsign = new ShopSign(type);
                    } else {
                        shopsign = new ShopSign();
                    }
                    shopsign.onPlace((Sign) event.getBlock().getState());
                    signs.put(event.getBlock().getLocation(), shopsign);
                    break;
                case "game":
                    SingleGameSign gamesign = new SingleGameSign(event.getBlock().getLocation());
                    gamesign.onPlace((Sign) event.getBlock().getState());
                    signs.put(event.getBlock().getLocation(), gamesign);
                    break;
                default:
                    event.getPlayer().sendMessage(Lang.getString("InvalidSignKey"));
                    break;
            }
            try {
                saveSigns();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @EventHandler
    public void onPlayerInteract(@NotNull PlayerInteractEvent event) {
        if (signs.containsKey(Objects.requireNonNull(event.getClickedBlock()).getLocation())) {
            signs.get(event.getClickedBlock().getLocation()).clickSign(event);
        }
    }

    private void saveSigns() throws IOException {
        File saveFile = new File(Deathrun.getInstance().getDataFolder() + "signs.json");
        if (!saveFile.exists()) {
            try {
                saveFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try (Writer writer = new FileWriter(saveFile)) {
            gson.toJson(signs, writer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Nullable
    private HashMap<Location, ISign> loadSigns(@NotNull File file) {
        try {
            JsonReader reader = new JsonReader(new FileReader(file));
            return gson.fromJson(reader, type);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void updateSigns(@NotNull UpdateInfo info) {
        if (Deathrun.getInstance().isBungeeMode()) {
            signs.values().forEach(iSign -> {
                if (iSign instanceof BungeeGameSign) {
                    BungeeGameSign bsign = (BungeeGameSign) iSign;
                    if (bsign.getServer().equalsIgnoreCase(info.getServer())) {
                        bsign.updateSign(info);
                    }
                }
            });
        } else {
            signs.values().forEach(iSign -> {
                if (iSign instanceof IGameSign) {
                    ((IGameSign) iSign).updateSign(info);
                }
            });
        }
    }
}