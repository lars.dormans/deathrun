package com.thecreepercow.deathrun.Lobby.Signs;

import com.thecreepercow.deathrun.Deathrun;
import com.thecreepercow.deathrun.Lang;
import com.thecreepercow.deathrun.Lobby.Shop.GUI.CategoryInventory;
import com.thecreepercow.deathrun.Lobby.Shop.ItemTypes;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.block.Sign;
import org.bukkit.event.player.PlayerInteractEvent;
import org.jetbrains.annotations.NotNull;

public class ShopSign implements ISign {
    ItemTypes type = ItemTypes.Banner;
    Location loc;

    public ShopSign(ItemTypes type) {
        this.type = type;
    }

    public ShopSign() {
    }

    public ShopSign(ItemTypes type, Location loc) {
        this.type = type;
        this.loc = loc;
    }

    @Override
    public void onPlace(@NotNull Sign block) {
        loc = block.getLocation();
        block.setLine(0, Lang.getString("sign-firstline"));
        block.setLine(1, Lang.getString("sign-shop"));
        block.setLine(2, ChatColor.DARK_PURPLE + type.getCategoryName());
        block.update();
    }

    @Override
    public void clickSign(@NotNull PlayerInteractEvent e) {
        Deathrun.getInstance().getLobby().getShopModule().openInventory(e.getPlayer(), new CategoryInventory(type));
    }

    @NotNull
    @Override
    public SignTypes getSignType() {
        return SignTypes.SHOP;
    }

    @Override
    public Location getLocation() {
        return loc;
    }
}
