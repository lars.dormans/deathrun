package com.thecreepercow.deathrun.Lobby.Shop;

import org.bukkit.inventory.ItemStack;

public interface IShopItem {
    String getName();

    int getPrice();

    String getDescription();

    ItemTypes getType();

    ItemStack getItemStack();
}
