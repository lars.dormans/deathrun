package com.thecreepercow.deathrun.Lobby.Signs;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import com.thecreepercow.deathrun.Deathrun;
import com.thecreepercow.deathrun.Lang;
import org.bukkit.Location;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class BungeeGameSign implements IGameSign {
    @Nullable
    private String serverName = null;
    private transient int playerAmount = 0;
    private final int maxPlayers = Deathrun.getInstance().getConfig().getInt("MaxPlayers");
    private Location blockloc;
    private transient Sign block;
    private transient String map = "Voting";

    public BungeeGameSign(String serverName) {
        this.serverName = serverName;
    }

    public BungeeGameSign(String serverName, @NotNull Location blockloc) {
        this.serverName = serverName;
        this.blockloc = blockloc;
        block = (Sign) blockloc.getBlock().getState();
    }

    public void onPlace(@NotNull Sign block) {
        this.block = block;
        blockloc = block.getLocation();
        serverName = block.getLine(2);
        block.setLine(0, Lang.getString("sign-firstline"));
        block.setLine(1, Lang.getString("gamesign-players")
                .replace("%playercurrent%", String.valueOf(playerAmount))
                .replace("%playermax%", String.valueOf(maxPlayers)));
        block.setLine(2, Lang.getString("gamesign-map").replace("%mapname%", map));
        block.update();
    }

    public void clickSign(@NotNull PlayerInteractEvent e) {
        //TODO Possible VIP join override
        Player player = e.getPlayer();
        if (playerAmount < maxPlayers) {
            ByteArrayDataOutput out = ByteStreams.newDataOutput();
            out.writeUTF("Connect");
            out.writeUTF(serverName);
            player.sendPluginMessage(Deathrun.getInstance(), "BungeeCord", out.toByteArray());
        } else {
            player.sendMessage(Lang.getString("gamefull"));
        }
    }

    public void updateSign(@NotNull UpdateInfo info) {
        playerAmount = info.getPlayers();
        map = info.getMap();
        block.setLine(1, Lang.getString("gamesign-players")
                .replace("%playercurrent%", String.valueOf(info.getPlayers()))
                .replace("%playermax%", String.valueOf(maxPlayers)));
        block.setLine(2, Lang.getString("gamesign-map").replace("%mapname%", info.getMap()));
        block.update();
    }

    @NotNull
    public SignTypes getSignType() {
        return SignTypes.GAME;
    }

    @Override
    public Location getLocation() {
        return blockloc;
    }

    @Nullable
    public String getServer() {
        return serverName;
    }
}
