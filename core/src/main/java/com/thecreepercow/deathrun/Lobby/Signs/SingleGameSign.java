package com.thecreepercow.deathrun.Lobby.Signs;

import com.thecreepercow.deathrun.Deathrun;
import com.thecreepercow.deathrun.Lang;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

public class SingleGameSign implements IGameSign {
    @Nullable
    private final World waitWorld = Bukkit.getWorld(Objects.requireNonNull(Deathrun.getInstance().getConfig().getString("waitWorld")));
    private transient int playerAmount = 0;
    private final int maxPlayers = Deathrun.getInstance().getConfig().getInt("MaxPlayers");
    private transient Sign block;
    private Location blockLoc;

    public SingleGameSign(@NotNull Location blockLoc) {
        this.blockLoc = blockLoc;
        block = (Sign) blockLoc.getBlock().getState();
    }

    public void updateSign(@NotNull UpdateInfo info) {
        block.setLine(1, Lang.getString("gamesign-players")
                .replace("%playercurrent%", String.valueOf(info.getPlayers()))
                .replace("%playermax%", String.valueOf(maxPlayers)));
        block.setLine(2, Lang.getString("gamesign-map").replace("%mapname%", info.getMap()));
        block.update();
    }

    public void onPlace(@NotNull Sign block) {
        this.block = block;
        blockLoc = block.getLocation();
        block.setLine(0, Lang.getString("sign-firstline"));
        block.setLine(1, Lang.getString("gamesign-players")
                .replace("%playercurrent%", String.valueOf(playerAmount))
                .replace("%playermax%", String.valueOf(maxPlayers)));
        block.setLine(2, Lang.getString("gamesign-map").replace("%mapname%", "Voting"));
        block.update();
    }

    public void clickSign(@NotNull PlayerInteractEvent e) {
        Player player = e.getPlayer();
        if (playerAmount < maxPlayers) {
            player.teleport(waitWorld.getSpawnLocation());
        } else {
            player.sendMessage(Lang.getString("gamefull"));
        }
    }

    @NotNull
    public SignTypes getSignType() {
        return SignTypes.GAME;
    }

    @Override
    public Location getLocation() {
        return blockLoc;
    }
}
