package com.thecreepercow.deathrun.Lobby.Shop.GUI;

import com.thecreepercow.deathrun.Deathrun;
import com.thecreepercow.deathrun.Lobby.Shop.ItemTypes;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.jetbrains.annotations.NotNull;

public class CategoryButton extends BaseButton {
    ItemTypes category;

    public CategoryButton(@NotNull ItemTypes category) {
        super(category.getCategoryItem());
        this.category = category;
    }

    @Override
    public void onClick(@NotNull InventoryClickEvent e) {
        CategoryInventory menu = new CategoryInventory(category);
        Player player = (Player) e.getWhoClicked();
        player.closeInventory();
        Deathrun.getInstance().getLobby().getShopModule().openInventory(player, menu);
    }
}
