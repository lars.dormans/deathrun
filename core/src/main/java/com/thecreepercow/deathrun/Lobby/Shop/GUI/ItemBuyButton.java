package com.thecreepercow.deathrun.Lobby.Shop.GUI;

import com.thecreepercow.deathrun.Deathrun;
import com.thecreepercow.deathrun.Lobby.Shop.IShopItem;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.jetbrains.annotations.NotNull;

public class ItemBuyButton extends BaseButton {
    IShopItem item;

    public ItemBuyButton(@NotNull IShopItem item) {
        super(item.getItemStack());
        this.item = item;
    }

    @Override
    public void onClick(@NotNull InventoryClickEvent e) {
        Player player = (Player) e.getWhoClicked();
        BuyItemInventory buy = new BuyItemInventory(item);
        Deathrun.getInstance().getLobby().getShopModule().openInventory(player, buy);
    }
}
