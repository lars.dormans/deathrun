package com.thecreepercow.deathrun.Lobby.Shop;

import com.thecreepercow.deathrun.Deathrun;
import com.thecreepercow.deathrun.Lobby.Shop.GUI.BaseInventory;
import com.thecreepercow.deathrun.Lobby.User;
import com.thecreepercow.deathrun.Module;
import com.thecreepercow.deathrun.Storage.MySQLDatabase;
import com.thecreepercow.deathrun.Util.ItemStackSerializer;
import javafx.util.Pair;
import org.bukkit.Particle;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ShopModule extends Module {
    @NotNull
    Map<ItemTypes, List<IShopItem>> shopItems = new HashMap<>();
    @NotNull
    Map<Player, BaseInventory> activeInventory = new HashMap<>();
    @NotNull
    Map<Pair<ItemTypes, Integer>, IShopItem> mappedShopItems = new HashMap<>();
    MySQLDatabase database;

    public ShopModule() {
        initialize();
    }

    @Override
    public void initialize() {
        database = Deathrun.getInstance().getDatabase();
        shopItems.put(ItemTypes.Banner, new ArrayList<>());
        shopItems.put(ItemTypes.JoinMSG, new ArrayList<>());
        shopItems.put(ItemTypes.Trail, new ArrayList<>());
        shopItems.put(ItemTypes.Custom, new ArrayList<>());
        loadItems();
    }

    public void openInventory(@NotNull Player player, @NotNull BaseInventory inventory) {
        player.closeInventory();
        activeInventory.put(player, inventory);
        player.openInventory(inventory.create(player));
    }

    @EventHandler(ignoreCancelled = true)
    public void onInventoryClick(@NotNull InventoryClickEvent event) {
        if (event.getWhoClicked() instanceof Player) {
            Player player = (Player) event.getWhoClicked();
            BaseInventory inventory = activeInventory.get(player);
            inventory.getButton(event.getSlot());
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void onInventoryClose(@NotNull InventoryCloseEvent event) {
        if (event.getPlayer() instanceof Player) {
            Player player = (Player) event.getPlayer();
            activeInventory.remove(player).onInventoryClose(event);
        }
    }

    private void loadItems() {
        PreparedStatement stmt = database.prepare("SELECT * FROM ?");
        try {
            stmt.setString(1, "banners");
            ResultSet data = stmt.executeQuery();
            while (data.next()) {
                String name = data.getString("name");
                String description = data.getString("description");
                int price = data.getInt("price");
                ItemStack banner = ItemStackSerializer.fromBase64(data.getString("banner"));
                BannerItem item = new BannerItem(name, description, price, banner);
                shopItems.get(ItemTypes.Banner).add(item);
                mappedShopItems.put(new Pair<>(ItemTypes.Banner, data.getInt("id")), item);
            }
            data.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            stmt.setString(1, "joinmsg");
            ResultSet data = stmt.executeQuery();
            while (data.next()) {
                String name = data.getString("name");
                String description = data.getString("description");
                int price = data.getInt("price");
                String msg = data.getString("msg");
                JoinMSGItem item = new JoinMSGItem(name, price, description, msg);
                shopItems.get(ItemTypes.JoinMSG).add(item);
                mappedShopItems.put(new Pair<>(ItemTypes.JoinMSG, data.getInt("id")), item);
            }
            data.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            stmt.setString(1, "trails");
            ResultSet data = stmt.executeQuery();
            while (data.next()) {
                String name = data.getString("name");
                String description = data.getString("description");
                int price = data.getInt("price");
                Particle particle = Particle.valueOf(data.getString("particlename"));
                TrailItem item = new TrailItem(name, price, description, particle);
                shopItems.get(ItemTypes.Trail).add(item);
                mappedShopItems.put(new Pair<>(ItemTypes.Trail, data.getInt("id")), item);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Nullable
    public List<IShopItem> getBoughtItems(@NotNull User player) {
        List<IShopItem> items = new ArrayList<>();
        PreparedStatement inventstmt = database.prepare("SELECT * FROM userinventory WHERE userId = ?");
        try {
            inventstmt.setInt(1, player.getId());
            ResultSet data = inventstmt.executeQuery();
            while (data.next()) {
                ItemTypes type = ItemTypes.valueOf(data.getString("itemType"));
                items.add(mappedShopItems.get(new Pair<>(type, data.getInt("itemId"))));
            }
            return items;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<IShopItem> getShopItems(ItemTypes type) {
        return shopItems.get(type);
    }

    public void addItem(@NotNull User user, @NotNull IShopItem item) {
        user.setTokens(user.getTokens() - item.getPrice());
        user.addBoughtItem(item);
    }

    @NotNull
    public List<String> getItemNames(@Nullable Player player) {
        User user = null;
        if (player != null){
            user = Deathrun.getInstance().getLobby().loadUser(player);
        }
        List<String> names = new ArrayList<>();
        for (ItemTypes type : ItemTypes.values()) {
            List<IShopItem> items = shopItems.get(type);
            items.forEach(iShopItem -> names.add(iShopItem.getName()));
            if (user!=null) {
                items.removeAll(user.getBoughtItems());
            }
        }
        return names;
    }
}
