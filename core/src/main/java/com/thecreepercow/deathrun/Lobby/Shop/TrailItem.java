package com.thecreepercow.deathrun.Lobby.Shop;

import com.thecreepercow.deathrun.Util.ParticleEffectMaterialUtil;
import org.bukkit.ChatColor;
import org.bukkit.Particle;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.jetbrains.annotations.NotNull;

import java.util.Collections;

public class TrailItem implements IShopItem {
    String name;
    int price;
    String description;
    Particle trail;

    public TrailItem(String name, int price, String description, Particle trail) {
        this.name = name;
        this.price = price;
        this.description = description;
        this.trail = trail;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getPrice() {
        return price;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @NotNull
    @Override
    public ItemTypes getType() {
        return ItemTypes.Trail;
    }

    @NotNull
    @Override
    public ItemStack getItemStack() {
        ItemStack item = new ItemStack(ParticleEffectMaterialUtil.getParticleMaterial(trail));
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(ChatColor.LIGHT_PURPLE + name);
        meta.setLore(Collections.singletonList(ChatColor.LIGHT_PURPLE + description));
        item.setItemMeta(meta);
        return item;
    }
}
