package com.thecreepercow.deathrun.Lobby.Shop;

import org.bukkit.ChatColor;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.jetbrains.annotations.NotNull;

import java.util.Collections;

public class BannerItem implements IShopItem {
    String name;
    String description;
    int price;
    ItemStack banner;


    public BannerItem(String name, String description, int price, @NotNull ItemStack banner) {
        this.name = name;
        this.description = description;
        this.price = price;
        if (banner.getType().name().contains("BANNER") && !(banner.getType().name().contains("WALL"))) {
            this.banner = banner;
        } else {
            throw new IllegalArgumentException("Banner is not a banner Material");
        }
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getPrice() {
        return price;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @NotNull
    @Override
    public ItemTypes getType() {
        return ItemTypes.Banner;
    }

    public ItemStack getBanner() {
        return banner;
    }

    @NotNull
    @Override
    public ItemStack getItemStack() {
        ItemStack rbanner = banner.clone();
        ItemMeta rbannermeta = rbanner.getItemMeta();
        rbannermeta.setDisplayName(ChatColor.LIGHT_PURPLE + name);
        rbannermeta.setLore(Collections.singletonList(ChatColor.LIGHT_PURPLE + description));
        rbanner.setItemMeta(rbannermeta);
        return rbanner;
    }
}
