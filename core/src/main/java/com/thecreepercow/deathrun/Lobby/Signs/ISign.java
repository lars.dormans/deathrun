package com.thecreepercow.deathrun.Lobby.Signs;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.bukkit.Location;
import org.bukkit.block.Sign;
import org.bukkit.event.player.PlayerInteractEvent;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Modifier;


public interface ISign {
    Gson gson = new GsonBuilder().excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT).enableComplexMapKeySerialization().create();

    void onPlace(Sign block);

    void clickSign(PlayerInteractEvent e);

    SignTypes getSignType();

    Location getLocation();

    static ISign fromString(String str, @NotNull Class<? extends ISign> type) {
        return gson.fromJson(str, type);
    }

    default String serialize() {
        return gson.toJson(this, this.getClass());
    }
}
