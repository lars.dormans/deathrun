package com.thecreepercow.deathrun.Lobby;

import co.aikar.commands.BukkitCommandExecutionContext;
import co.aikar.commands.MinecraftMessageKeys;
import co.aikar.commands.contexts.IssuerAwareContextResolver;
import com.thecreepercow.deathrun.Deathrun;
import com.thecreepercow.deathrun.Game.VisibilitySetting;
import com.thecreepercow.deathrun.Lobby.Shop.IShopItem;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class User {
    private int id;
    private UUID uuid;
    private int gamesplayed = 0;
    private int winsrunner = 0;
    private int winsdeath = 0;
    private int loses = 0;
    private int tokens = 0;
    private int kills = 0;
    private VisibilitySetting visibilitySetting = VisibilitySetting.visible;
    @NotNull
    private List<IShopItem> boughtItems = new ArrayList<>();

    public User(int id, UUID uuid) {
        this.id = id;
        this.uuid = uuid;
    }

    public int getGamesplayed() {
        return gamesplayed;
    }

    public void setGamesplayed(int gamesplayed) {
        this.gamesplayed = gamesplayed;
    }

    public int getWinsrunner() {
        return winsrunner;
    }

    public void setWinsrunner(int winsrunner) {
        this.winsrunner = winsrunner;
    }

    public int getWinsdeath() {
        return winsdeath;
    }

    public void setWinsdeath(int winsdeath) {
        this.winsdeath = winsdeath;
    }

    public int getLoses() {
        return loses;
    }

    public void setLoses(int loses) {
        this.loses = loses;
    }

    public int getTokens() {
        return tokens;
    }

    public void setTokens(int tokens) {
        this.tokens = tokens;
    }

    public int getKills() {
        return kills;
    }

    public void setKills(int kills) {
        this.kills = kills;
    }

    public VisibilitySetting getVisibilitySetting() {
        return visibilitySetting;
    }

    public void setVisibilitySetting(VisibilitySetting visibilitySetting) {
        this.visibilitySetting = visibilitySetting;
    }

    public int getId() {
        return id;
    }

    public UUID getUuid() {
        return uuid;
    }

    @NotNull
    public List<IShopItem> getBoughtItems() {
        return boughtItems;
    }

    public void addBoughtItem(IShopItem item) {
        boughtItems.add(item);
    }

    void setId(int id) {
        this.id = id;
    }

    @NotNull
    public static IssuerAwareContextResolver<User, BukkitCommandExecutionContext> getResolver() {
        return con -> {
            if (con.hasFlag("noinput")) {
                if (con.getSender() instanceof Player) {
                    return Deathrun.getInstance().getLobby().loadUser((Player) con.getSender());
                }
            } else {
                String playerName = con.popFirstArg();
                User user = Deathrun.getInstance().getLobby().loadUser(Bukkit.getPlayer(playerName));
                if (user == null) {
                    con.getIssuer().sendError(MinecraftMessageKeys.NO_PLAYER_FOUND);
                } else {
                    return user;
                }
            }
            return null;
        };
    }
}
