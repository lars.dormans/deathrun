package com.thecreepercow.deathrun.Lobby.Shop.GUI;

import com.thecreepercow.deathrun.Deathrun;
import com.thecreepercow.deathrun.Lang;
import com.thecreepercow.deathrun.Lobby.LobbyMode;
import com.thecreepercow.deathrun.Lobby.Shop.IShopItem;
import com.thecreepercow.deathrun.Lobby.Shop.ItemTypes;
import com.thecreepercow.deathrun.Lobby.User;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class CategoryInventory extends BaseInventory {
    ItemTypes category;
    final LobbyMode lobbyMode = Deathrun.getInstance().getLobby();

    public CategoryInventory(@NotNull ItemTypes category) {
        super(45, Lang.getString("GUIShopTitle").replace("%type%", category.getCategoryName()));
        this.category = category;
    }

    private void setupDefault() {
        int position = 0;
        for (ItemTypes type : ItemTypes.values()) {
            if (type.equals(category)) {
                ItemStack activeItem = type.getCategoryItem();
                ItemMeta activeMeta = activeItem.getItemMeta();
                activeMeta.addEnchant(Enchantment.DURABILITY, 1, true);
                activeMeta.addItemFlags(ItemFlag.values());
                activeItem.setItemMeta(activeMeta);
                setItem(position, new BackgroundButton(activeItem));
                position += 9;
            } else {
                setItem(position, new CategoryButton(type));
                position += 9;
            }
        }
        ItemStack background = new ItemStack(Material.WHITE_STAINED_GLASS_PANE);
        for (int i = 1; i <= 45; i += 9) {
            setItem(i, new BackgroundButton(background));
        }

    }

    @NotNull
    @Override
    public Inventory create(Player player) {
        AtomicInteger pos = new AtomicInteger(2);
        setupDefault();
        User user = lobbyMode.loadUser(player);
        List<IShopItem> boughtItems = user.getBoughtItems();
        lobbyMode.getShopModule().getShopItems(category).forEach(item -> {
            if (boughtItems.contains(item)) {
                boolean addedItem = false;
                ItemStack boughtItem = item.getItemStack();
                ItemMeta boughtMeta = boughtItem.getItemMeta();
                boughtMeta.addEnchant(Enchantment.DURABILITY, 1, true);
                boughtMeta.addItemFlags(ItemFlag.values());
                boughtItem.setItemMeta(boughtMeta);
                BackgroundButton button = new BackgroundButton(boughtItem);
                while (!addedItem) {
                    addedItem = setItem(pos.get(), button);
                    pos.addAndGet(1);
                }
            } else {
                boolean addedItem = false;
                ItemBuyButton button = new ItemBuyButton(item);
                while (!addedItem) {
                    addedItem = setItem(pos.get(), button);
                    pos.addAndGet(1);
                }
            }
        });
        return finalCreation();
    }

    @Override
    boolean allowOverride() {
        return false;
    }

    @Override
    public void onInventoryClose(InventoryCloseEvent event) {

    }
}
