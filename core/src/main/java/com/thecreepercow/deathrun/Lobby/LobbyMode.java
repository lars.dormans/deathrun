package com.thecreepercow.deathrun.Lobby;

import com.thecreepercow.deathrun.Deathrun;
import com.thecreepercow.deathrun.Game.VisibilitySetting;
import com.thecreepercow.deathrun.Lobby.Shop.ShopModule;
import com.thecreepercow.deathrun.Lobby.Signs.SignManager;
import com.thecreepercow.deathrun.Module;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.UUID;

public class LobbyMode extends Module {
    private ShopModule shopModule;
    private SignManager signManager;
    private HashMap<UUID, User> users;

    @Override
    public void initialize() {
        shopModule = new ShopModule();
        signManager = new SignManager();
    }

    public ShopModule getShopModule() {
        return shopModule;
    }

    public SignManager getSignManager() {
        return signManager;
    }

    @Nullable
    public User loadUser(@Nullable Player player) {
        if (player == null) {
            return null;
        }
        if (users.containsKey(player.getUniqueId())) {
            return users.get(player.getUniqueId());
        } else {
            return loadUserFromDB(player);
        }
    }

    @Nullable
    private User loadUserFromDB(@NotNull Player player) {
        PreparedStatement stmt = Deathrun.getInstance().getDatabase().prepare("SELECT * FROM users WHERE uuid = ?");
        try {
            stmt.setString(1, player.getUniqueId().toString());
            ResultSet data = stmt.executeQuery();
            if (data.first()) {
                User user = new User(data.getInt("id"), player.getUniqueId());
                user.setGamesplayed(data.getInt("games"));
                user.setKills(data.getInt("kills"));
                user.setLoses(data.getInt("loses"));
                user.setTokens(data.getInt("tokens"));
                user.setVisibilitySetting(VisibilitySetting.valueOf(data.getString("visibilitySetting")));
                user.setWinsdeath(data.getInt("winsdeath"));
                user.setWinsrunner(data.getInt("winsrunner"));
                shopModule.getBoughtItems(user).forEach(user::addBoughtItem);
                users.put(player.getUniqueId(), user);
                return user;
            } else {
                User user = new User(-1, player.getUniqueId());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void saveUserToDB(@NotNull User user) {
        if (user.getId() == -1) {
            PreparedStatement insertSTMT = Deathrun.getInstance().getDatabase().prepare("INSERT INTO `users`(`uuid`, `games`, `winsrunner`, `winsdeath`, `loses`, `tokens`, `kills`, `visibilitySetting`) VALUES (?,?,?,?,?,?,?,?)");
            try {
                insertSTMT.setString(1, user.getUuid().toString());
                insertSTMT.setInt(2, user.getGamesplayed());
                insertSTMT.setInt(3, user.getWinsrunner());
                insertSTMT.setInt(4, user.getWinsdeath());
                insertSTMT.setInt(5, user.getLoses());
                insertSTMT.setInt(6, user.getTokens());
                insertSTMT.setInt(7, user.getKills());
                insertSTMT.setInt(8, user.getVisibilitySetting().getDbid());
                insertSTMT.executeUpdate();
                ResultSet updrtu = insertSTMT.getGeneratedKeys();
                if (updrtu.next()) {
                    user.setId(updrtu.getInt(1));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            PreparedStatement updateSTMT = Deathrun.getInstance().getDatabase().prepare("UPDATE `users` SET `games`= ?,`winsrunner`= ?,`winsdeath`= ?,`loses`= ?,`tokens`= ?,`kills`= ?,`visibilitySetting`= ? WHERE `id` = ?");
            try {
                updateSTMT.setInt(1, user.getGamesplayed());
                updateSTMT.setInt(2, user.getWinsrunner());
                updateSTMT.setInt(3, user.getWinsdeath());
                updateSTMT.setInt(4, user.getLoses());
                updateSTMT.setInt(5, user.getTokens());
                updateSTMT.setInt(6, user.getKills());
                updateSTMT.setInt(7, user.getVisibilitySetting().getDbid());
                updateSTMT.setInt(8, user.getId());
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
    }
}
