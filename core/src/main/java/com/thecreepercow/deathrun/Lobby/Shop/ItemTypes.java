package com.thecreepercow.deathrun.Lobby.Shop;

import com.thecreepercow.deathrun.Lang;
import com.thecreepercow.deathrun.Util.StaticItemStack;
import org.bukkit.inventory.ItemStack;

public enum ItemTypes {
    Banner("Banner", StaticItemStack.bannerCategory),
    JoinMSG("Join Message", StaticItemStack.joinMSGCategory),
    Trail("Trails", StaticItemStack.trailCategory),
    Custom(Lang.getString("SPECIALCustomCategoryName"), StaticItemStack.customCategory);

    private String categoryName;
    private ItemStack categoryItem;

    public String getCategoryName() {
        return categoryName;
    }

    public ItemStack getCategoryItem() {
        return categoryItem;
    }

    ItemTypes(String categoryName, ItemStack categoryItem) {
        this.categoryName = categoryName;
        this.categoryItem = categoryItem;
    }
}
