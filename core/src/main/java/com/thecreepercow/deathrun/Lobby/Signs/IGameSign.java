package com.thecreepercow.deathrun.Lobby.Signs;

public interface IGameSign extends ISign {
    void updateSign(UpdateInfo info);
}
