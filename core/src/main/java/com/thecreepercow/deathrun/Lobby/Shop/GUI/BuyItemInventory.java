package com.thecreepercow.deathrun.Lobby.Shop.GUI;

import com.thecreepercow.deathrun.Deathrun;
import com.thecreepercow.deathrun.Lang;
import com.thecreepercow.deathrun.Lobby.Shop.IShopItem;
import com.thecreepercow.deathrun.Lobby.User;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public class BuyItemInventory extends BaseInventory {
    IShopItem item;

    public BuyItemInventory(@NotNull IShopItem item) {
        super(9, Lang.getString("GUIBuyTitle").replace("%itemname%", item.getName()));
        this.item = item;
    }

    @NotNull
    @Override
    public Inventory create(@NotNull Player player) {
        User user = Deathrun.getInstance().getLobby().loadUser(player);
        if (user.getBoughtItems().contains(item)) {
            player.closeInventory();
            player.sendMessage(ChatColor.RED + "ERROR: You already own this item you cant buy it again, Please contact server administration if you dont own the item");
        }
        ItemStack buyItem = item.getItemStack();
        ItemMeta buyMeta = buyItem.getItemMeta();
        List<String> buyLore = buyMeta.getLore();
        buyLore.add(ChatColor.AQUA + String.valueOf(item.getPrice()));
        buyMeta.setLore(buyLore);
        buyItem.setItemMeta(buyMeta);
        BaseButton button = new BackgroundButton(buyItem);
        BaseButton returnButton = new ReturnButton(new CategoryInventory(item.getType()));
        ItemStack confirmItem = new ItemStack(Material.EMERALD_BLOCK);
        ItemMeta confirmMeta = confirmItem.getItemMeta();
        confirmMeta.setDisplayName(Lang.getString("GUIButtonConfirm"));
        confirmItem.setItemMeta(confirmMeta);
        BaseButton confirmButton = new BaseButton(confirmItem) {
            @Override
            public void onClick(InventoryClickEvent e) {
                if (user.getTokens() >= item.getPrice()) {
                    Deathrun.getInstance().getLobby().getShopModule().addItem(user, item);
                } else {
                    player.closeInventory();
                    player.sendMessage(Lang.getString("BuyNotEnoughTokens"));
                }
            }
        };
        setItem(2, returnButton);
        setItem(4, button);
        setItem(6, confirmButton);
        return finalCreation();
    }

    @Override
    boolean allowOverride() {
        return false;
    }

    @Override
    public void onInventoryClose(InventoryCloseEvent event) {

    }
}
