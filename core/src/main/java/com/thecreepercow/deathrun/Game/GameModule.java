package com.thecreepercow.deathrun.Game;

import com.thecreepercow.deathrun.Module;

import java.util.EventListener;

public class GameModule extends Module implements EventListener {
    PreGameManager preGameManager;
    GameManager gameManager;
    ScoreboardManager scoreboardManager;

    @Override
    public void initialize() {
        preGameManager = new PreGameManager();
        preGameManager.initialize();
        gameManager = new GameManager();
        gameManager.initialize();
        scoreboardManager = new ScoreboardManager();
        scoreboardManager.initialize();
    }

    public PreGameManager getPreGameManager() {
        return preGameManager;
    }

    public GameManager getGameManager() {
        return gameManager;
    }

    public ScoreboardManager getScoreboardManager() {
        return scoreboardManager;
    }
}
