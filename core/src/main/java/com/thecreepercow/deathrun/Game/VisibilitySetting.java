package com.thecreepercow.deathrun.Game;

import org.jetbrains.annotations.NotNull;

public enum VisibilitySetting {
    visible(1), transparent(2), invisible(3);

    private int dbid;

    VisibilitySetting(int dbid) {
        this.dbid = dbid;
    }

    public int getDbid() {
        return dbid;
    }

    @NotNull
    public static VisibilitySetting fromDBId(int id) {
        switch (id) {
            case 2:
                return transparent;
            case 3:
                return invisible;
            default:
                return visible;
        }
    }
}
