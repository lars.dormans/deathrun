package com.thecreepercow.deathrun.Game;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import com.thecreepercow.deathrun.Deathrun;
import com.thecreepercow.deathrun.Lang;
import com.thecreepercow.deathrun.Lobby.LobbyMode;
import com.thecreepercow.deathrun.Lobby.Signs.UpdateInfo;
import com.thecreepercow.deathrun.Lobby.User;
import com.thecreepercow.deathrun.Module;
import com.thecreepercow.deathrun.Setup.MapData;
import com.thecreepercow.deathrun.Setup.MapManager;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public class PreGameManager extends Module implements EventListener {
    int playerCount = 0;
    int maxPlayers = Deathrun.getInstance().getConfig().getInt("MaxPlayers");
    int minPlayers = Deathrun.getInstance().getConfig().getInt("MinPlayers");
    int Deaths = Deathrun.getInstance().getConfig().getInt("DeathAmount");
    HashMap<Player, GamePlayer> players = new HashMap<>();
    MapManager mapManager;
    LobbyMode lobby;
    HashMap<String, Integer> votes = new HashMap<>();
    String votedMap = "Voting";
    GameStatus status = GameStatus.Voting;
    int startTimer = Deathrun.getInstance().getConfig().getInt("StartTimer");
    BukkitTask voteReminder;
    BukkitRunnable startTimerTask = new BukkitRunnable(){
        @Override
        public void run() {
            voteReminder.cancel();
            AtomicReference<String> voted = new AtomicReference<>(null);
            AtomicInteger highest = new AtomicInteger(0);
            votes.forEach((key, value) -> {
                if (value> highest.get()){
                    voted.set(key);
                    highest.set(value);
                }
            });
            if (voted.get()==null){
                List<String> votemaps = new ArrayList<>(votes.keySet());
                Random rnd = new Random();
                int r = rnd.nextInt(votemaps.size());
                voted.set(votemaps.get(r));
            }
            votedMap = voted.get();
            players.keySet().forEach(player -> {
                player.sendMessage(votedMap+Lang.getString("MapVoted"));
            });
            mapManager.setActiveMap(votedMap);
            List<Player> playersList = new ArrayList<>(players.keySet());
            Random rnd = new Random();
            for (int i = 0; i < Deaths ; i++) {
                int r = rnd.nextInt(playersList.size());
                Player p = playersList.get(i);
                players.get(p).setDeath(true);
            }
            status = GameStatus.Game;
            Deathrun.getInstance().getGame().getGameManager().inintalTeleportPlayers();
        }
    };
    @Override
    public void initialize() {
        mapManager = Deathrun.getInstance().getSetup().getMapManager();
        lobby = Deathrun.getInstance().getLobby();
        mapManager.getMaps().forEach(s -> votes.put(s,0));
        voteReminder = new BukkitRunnable(){
            @Override
            public void run() {
                players.keySet().forEach(player -> {
                    player.sendMessage(Lang.getString("VoteMapMessage"));
                    int i = 1;
                    votes.keySet().forEach(s -> {
                        MapData data = mapManager.getMap(s);
                        TextComponent msg = new TextComponent(i+": "+data.getName());
                        msg.setColor(ChatColor.GOLD);
                        msg.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/dtr vote "+i));
                        msg.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new TextComponent[]{new TextComponent("Vote for "+ s)}));
                        player.spigot().sendMessage(msg);
                    });
                });
            }
        }.runTaskTimer(Deathrun.getInstance(), 0,60*20);

    }

    @EventHandler(ignoreCancelled = true)
    public void onPlayerTeleport(PlayerTeleportEvent event) {
        //Single
        if (event.getTo().getWorld().getName().equalsIgnoreCase(Deathrun.getInstance().getConfig().getString("waitWorld"))&&!Deathrun.getInstance().isBungeeMode()){
            if(playerCount==maxPlayers&&!event.getPlayer().hasPermission("DTR.full")){
                event.getPlayer().sendMessage(Lang.getString("GameFull"));
                event.setCancelled(true);
                return;
            }
            playerCount+=1;
            players.put(event.getPlayer(), new GamePlayer(lobby.loadUser(event.getPlayer()),event.getPlayer()));
            lobby.getSignManager().updateSigns(new UpdateInfo(playerCount, votedMap, status));
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void onPlayerJoin(PlayerJoinEvent event) {
        //Bungee
        if (Deathrun.getInstance().isBungeeMode()) {
            if (playerCount==maxPlayers&&!event.getPlayer().hasPermission("DTR.full")){
                event.getPlayer().sendMessage(Lang.getString("GameFull"));
                String server = Deathrun.getInstance().getConfig().getString("Bungee.lobby-server");
                ByteArrayDataOutput out = ByteStreams.newDataOutput();
                out.writeUTF("Connect");
                out.writeUTF(server);
                event.getPlayer().sendPluginMessage(Deathrun.getInstance(),"BungeeCord",out.toByteArray());
                return;
            }
            event.getPlayer().teleport(Bukkit.getWorld(Deathrun.getInstance().getConfig().getString("waitWorld")).getSpawnLocation());
            playerCount += 1;
            players.put(event.getPlayer(),new GamePlayer(lobby.loadUser(event.getPlayer()),event.getPlayer()));
            if (Deathrun.getInstance().getServerName() == null) {
                UpdateInfo.getServerName(event.getPlayer());
            }else{
                UpdateInfo.send(new UpdateInfo(Deathrun.getInstance().getServerName(),playerCount, votedMap, status),event.getPlayer());
            }
            if (playerCount>minPlayers){
                startTimerTask.runTaskLater(Deathrun.getInstance(), startTimer*20);
            }
        }
    }

    public void playerLeave(Player player){
        players.remove(player);
        playerCount-=1;
        lobby.getSignManager().updateSigns(new UpdateInfo(playerCount,votedMap,status));
        if (playerCount<minPlayers){
            try {
                startTimerTask.cancel();
            }catch (IllegalStateException ignored){

            }
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void onPlayerQuit(PlayerQuitEvent event) {
        if (players.containsKey(event.getPlayer())){
            User user = players.remove(event.getPlayer()).getUser();
            playerCount-=1;
            if (Deathrun.getInstance().isBungeeMode()){
                //Bungee
                UpdateInfo.send(new UpdateInfo(Deathrun.getInstance().getServerName(), playerCount, votedMap, status),event.getPlayer());
            }else{
                //Single
                lobby.getSignManager().updateSigns(new UpdateInfo(playerCount,votedMap,status));
            }
            if (playerCount<minPlayers){
                try {
                    startTimerTask.cancel();
                } catch (IllegalStateException ignored){

                }
            }
        }
    }

    public int getPlayerCount() {
        return playerCount;
    }

    public HashMap<Player, GamePlayer> getPlayers() {
        return players;
    }
}
