package com.thecreepercow.deathrun.Game;

import com.thecreepercow.deathrun.Deathrun;
import com.thecreepercow.deathrun.Lobby.User;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class GamePlayer {
    User user;
    Player player;
    boolean isDeath = false;
    boolean hasVoted = false;
    int lives = Deathrun.getInstance().getConfig().getInt("Game.StartingLives");
    Location respawnPoint;
    public GamePlayer(User user, Player player)
    {
        this.user = user;
        this.player = player;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isDeath() {
        return isDeath;
    }

    public void setDeath(boolean death) {
        isDeath = death;
    }

    public int getLives() {
        return lives;
    }

    public void setLives(int lives) {
        this.lives = lives;
    }

    public boolean isHasVoted() {
        return hasVoted;
    }

    public void setHasVoted(boolean hasVoted) {
        this.hasVoted = hasVoted;
    }

    public Location getRespawnPoint() {
        return respawnPoint;
    }

    public void setRespawnPoint(Location respawnPoint) {
        this.respawnPoint = respawnPoint;
    }

    public Player getPlayer() {
        return player;
    }
}
