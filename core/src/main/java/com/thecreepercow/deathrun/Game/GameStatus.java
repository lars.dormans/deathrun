package com.thecreepercow.deathrun.Game;

public enum GameStatus {
    Voting, PreGame, Game, Finished
}
