package com.thecreepercow.deathrun.Game;

import com.thecreepercow.deathrun.Deathrun;
import com.thecreepercow.deathrun.Module;
import com.thecreepercow.deathrun.Setup.MapManager;
import org.bukkit.Location;

import java.util.EventListener;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class GameManager extends Module implements EventListener {
    PreGameManager pregame;
    MapManager mapManager;
    @Override
    public void initialize() {
        pregame = Deathrun.getInstance().getGame().getPreGameManager();
        mapManager = Deathrun.getInstance().getSetup().getMapManager();
    }

    public void inintalTeleportPlayers(){
        List<Location> spawns = mapManager.getActiveMap().getSpawns();
        List<Location> deathSpawns = mapManager.getActiveMap().getDeathSpawns();
        AtomicInteger s = new AtomicInteger(0);
        AtomicInteger sd = new AtomicInteger(0);
        pregame.getPlayers().values().forEach(player -> {
            if (s.get() > spawns.size()){
                s.set(0);
            }
            if (sd.get() > deathSpawns.size()){
                sd.set(0);
            }
            if (!player.isDeath()){
                player.getPlayer().teleport(spawns.get(s.get()));
                s.addAndGet(1);
            }else{
                player.getPlayer().teleport(deathSpawns.get(sd.get()));
                sd.addAndGet(1);
            }
        });
    }
}
