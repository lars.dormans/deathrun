package com.thecreepercow.deathrun.Util;

import org.bukkit.Material;
import org.bukkit.Particle;
import org.jetbrains.annotations.NotNull;

public final class ParticleEffectMaterialUtil {
    @NotNull
    public static Material getParticleMaterial(@NotNull Particle particle) {
        switch (particle) {
            case EXPLOSION_NORMAL:
            case EXPLOSION_LARGE:
            case EXPLOSION_HUGE:
                return Material.TNT;
            case FIREWORKS_SPARK:
            case FLASH:
                return Material.FIREWORK_ROCKET;
            case CRIT:
                return Material.DIAMOND_SWORD;
            case SMOKE_NORMAL:
            case SMOKE_LARGE:
            case CLOUD:
                return Material.FIREWORK_STAR;
            case DRIP_WATER:
            case WATER_DROP:
            case FALLING_WATER:
                return Material.WATER_BUCKET;
            case DRIP_LAVA:
            case LAVA:
            case FALLING_LAVA:
            case LANDING_LAVA:
                return Material.LAVA_BUCKET;
            case VILLAGER_ANGRY:
                return Material.COAL;
            case VILLAGER_HAPPY:
            case COMPOSTER:
                return Material.EMERALD;
            case NOTE:
                return Material.NOTE_BLOCK;
            case PORTAL:
                return Material.OBSIDIAN;
            case ENCHANTMENT_TABLE:
                return Material.ENCHANTING_TABLE;
            case FLAME:
                return Material.BLAZE_POWDER;
            case REDSTONE:
                return Material.REDSTONE;
            case SNOWBALL:
            case SNOW_SHOVEL:
                return Material.SNOWBALL;
            case SLIME:
            case SNEEZE:
                return Material.SLIME_BALL;
            case HEART:
                return Material.POTION;
            case BARRIER:
                return Material.BARRIER;
            case DRAGON_BREATH:
                return Material.DRAGON_BREATH;
            case END_ROD:
                return Material.END_ROD;
            case CAMPFIRE_COSY_SMOKE:
                return Material.FLINT_AND_STEEL;
            case CAMPFIRE_SIGNAL_SMOKE:
                return Material.HAY_BLOCK;
            default:
                return Material.DIRT;
        }
    }
}
