package com.thecreepercow.deathrun.Util;

import com.thecreepercow.deathrun.Lang;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.jetbrains.annotations.NotNull;

public class StaticItemStack {
    // Due to itemmeta limitation we initialize the itemstacks here for later use in the project (i know i know hush)
    private static boolean initalized = false;
    @NotNull
    public static ItemStack bannerCategory = new ItemStack(Material.WHITE_BANNER);
    @NotNull
    public static ItemStack joinMSGCategory = new ItemStack(Material.EMERALD);
    @NotNull
    public static ItemStack trailCategory = new ItemStack(Material.BLAZE_POWDER);
    @NotNull
    public static ItemStack customCategory = new ItemStack(Material.valueOf(Lang.getString("SPECIALCustomCategoryItem")));
    @NotNull
    public static ItemStack setupStick = new ItemStack(Material.STICK);

    public static void initialize() {
        if (!initalized) {
            ItemMeta bannerMeta = bannerCategory.getItemMeta();
            bannerMeta.setDisplayName(Lang.getString("GUIBannerItemName"));
            bannerCategory.setItemMeta(bannerMeta);
            ItemMeta joinMSGMeta = joinMSGCategory.getItemMeta();
            joinMSGMeta.setDisplayName(Lang.getString("GUIJoinMSGItemName"));
            joinMSGCategory.setItemMeta(joinMSGMeta);
            ItemMeta trailMeta = trailCategory.getItemMeta();
            trailMeta.setDisplayName(Lang.getString("GUITrailItemName"));
            trailCategory.setItemMeta(trailMeta);
            ItemMeta customMeta = customCategory.getItemMeta();
            customMeta.setDisplayName(Lang.getString("SPECIALCustomCategoryItemDisplay"));
            customCategory.setItemMeta(customMeta);
            ItemMeta setupMeta = setupStick.getItemMeta();
            setupMeta.setDisplayName(Lang.getString("SetupTool"));
            setupStick.setItemMeta(setupMeta);
            initalized = true;
        }
    }
}
