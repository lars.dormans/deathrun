package com.thecreepercow.deathrun.Util;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class LocationUtil {
    public static boolean isInArea(@NotNull Location l, @NotNull Location l1, @NotNull Location l2) {

        double x1 = Math.min(l1.getX(), l2.getX());
        double y1 = Math.min(l1.getY(), l2.getY());
        double z1 = Math.min(l1.getZ(), l2.getZ());

        double x2 = Math.max(l1.getX(), l2.getX());
        double y2 = Math.max(l1.getY(), l2.getY());
        double z2 = Math.max(l1.getZ(), l2.getZ());

        return l.getX() >= x1 && l.getX() <= x2 &&
                l.getY() >= y1 && l.getY() <= y2 &&
                l.getZ() >= z1 && l.getZ() <= z2;
    }
    @NotNull
    public static List<Block> getArea(@NotNull Location l1, @NotNull Location l2){
        List<Block> bl = new ArrayList<>();
        int topBlockX = (Math.max(l1.getBlockX(), l2.getBlockX()));
        int bottomBlockX = (Math.min(l1.getBlockX(), l2.getBlockX()));

        int topBlockY = (Math.max(l1.getBlockY(), l2.getBlockY()));
        int bottomBlockY = (Math.min(l1.getBlockY(), l2.getBlockY()));

        int topBlockZ = (Math.max(l1.getBlockZ(), l2.getBlockZ()));
        int bottomBlockZ = (Math.min(l1.getBlockZ(), l2.getBlockZ()));

        for(int x = bottomBlockX; x <= topBlockX; x++)
        {
            for(int z = bottomBlockZ; z <= topBlockZ; z++)
            {
                for(int y = bottomBlockY; y <= topBlockY; y++)
                {
                    Block block = l1.getWorld().getBlockAt(x, y, z);
                    if (isNotAir(block)){
                        bl.add(block);
                    }
                }
            }
        }
        return bl;
    }
    public static boolean isNotAir(@NotNull Block block){
        return !block.getType().equals(Material.AIR) && !block.getType().equals(Material.CAVE_AIR) && !block.getType().equals(Material.VOID_AIR);
    }
}
