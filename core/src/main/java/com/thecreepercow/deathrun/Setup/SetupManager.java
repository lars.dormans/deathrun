package com.thecreepercow.deathrun.Setup;

import com.thecreepercow.deathrun.Deathrun;
import com.thecreepercow.deathrun.Lang;
import com.thecreepercow.deathrun.Module;
import com.thecreepercow.deathrun.Setup.Interactables.FinishInteractable;
import com.thecreepercow.deathrun.Setup.Interactables.Interactable;
import com.thecreepercow.deathrun.Setup.Interactables.PotionInteractable;
import com.thecreepercow.deathrun.Setup.Interactables.TeleportInteractable;
import com.thecreepercow.deathrun.Setup.Traps.*;
import com.thecreepercow.deathrun.Util.StaticItemStack;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.EventListener;
import java.util.HashMap;
import java.util.List;

public class SetupManager extends Module implements EventListener {
    @NotNull
    HashMap<Player, ArrayList<Location>> setupStickLocations = new HashMap<>();

    @Override
    public void initialize() {

    }

    @EventHandler(ignoreCancelled = true)
    public void onPlayerInteract(@NotNull PlayerInteractEvent event) {
        Player player = event.getPlayer();
        if (player.isSneaking()){
            setupStickLocations.get(player).clear();
            player.sendMessage(Lang.getString("SetupToolCleared"));
        }else {
            if (!event.getClickedBlock().getType().isAir()){
                setupStickLocations.get(player).add(event.getClickedBlock().getLocation());
                player.sendMessage(Lang.getString("SetupToolAdded"));
            }
        }
    }

    public void giveTool(@NotNull Player player){
        setupStickLocations.put(player,new ArrayList<>());
        player.getInventory().addItem(StaticItemStack.setupStick);
    }

    public void createTrap(@NotNull TrapList trap, String trapName, Player player){
        List<Location> locs = setupStickLocations.get(player);
        ITrap ctrap = null;
        if (locs.size()<2){
            player.sendMessage(Lang.getString("SetupNoLocations"));
        }
        switch (trap){
            case BlockTrap:
                ctrap = new BlockTrap(locs);
                break;
            case BridgeTrap:
                if (locs.size()>2){
                    player.sendMessage(Lang.getString("SetupTooManyLocations"));
                    break;
                }else {
                ctrap = new BridgeTrap(locs.get(0), locs.get(1));
                }
                break;
            case DispenserTrap:
                ctrap = new DispenserTrap(locs);
                break;
            case WallTrap:
                if (locs.size()>2) {
                    player.sendMessage(Lang.getString("SetupTooManyLocations"));
                    break;
                }else {
                    ctrap = new WallTrap(locs.get(0), locs.get(1));
                }
                break;
        }
        setupStickLocations.get(player).clear();
        MapData data = Deathrun.getInstance().getSetup().getMapManager().getMap(player.getWorld().getName());
        if (ctrap!=null) {
            data.addTrap(trapName, ctrap);
        }
    }
    public void createInteractable(int type, Player player, @Nullable String extra){
        Interactable interactable = null;
        switch (type){
            case 1:
                //Teleport
                interactable = new TeleportInteractable(setupStickLocations.get(player).get(1));
                break;
            case 2:
                //Finish
                interactable = new FinishInteractable();
                break;
            case 3:
                //Potion
                if (extra!=null) {
                    interactable = new PotionInteractable(extra);
                }
        }

        MapData data = Deathrun.getInstance().getSetup().getMapManager().getActiveMap();
        if (interactable!=null) {
            data.addInteractable(setupStickLocations.get(player).get(0), interactable);
        }
        setupStickLocations.get(player).clear();
    }

    public void setSpawns(Player player){
        MapData data = Deathrun.getInstance().getSetup().getMapManager().getActiveMap();
        data.setSpawns(setupStickLocations.get(player));
        setupStickLocations.get(player).clear();
    }
    public void setDeathSpawns(Player player){
        MapData data = Deathrun.getInstance().getSetup().getMapManager().getActiveMap();
        data.setDeathSpawns(setupStickLocations.get(player));
        setupStickLocations.get(player).clear();
    }

    @EventHandler(ignoreCancelled = true)
    public void onSignChange(SignChangeEvent event) {
        String[] lines = event.getLines();
        if (lines[0].equalsIgnoreCase("[DTR]")&&lines[1].equalsIgnoreCase("trap")){
            MapData data = Deathrun.getInstance().getSetup().getMapManager().getActiveMap();
            String trapName = lines[3];
            ITrap trap = data.getTraps().get(trapName);
            TrapSign sign = new TrapSign(trapName, event.getBlock().getLocation());
            event.setLine(0,ChatColor.DARK_PURPLE+"[DTR]");
            event.setLine(1, ChatColor.DARK_AQUA+trap.getTrapType());
            event.setLine(2, ChatColor.GOLD+String.valueOf(trap.getCooldown()));
            data.addSign(sign);
        }
    }
}
