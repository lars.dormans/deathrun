package com.thecreepercow.deathrun.Setup.Traps;

import com.thecreepercow.deathrun.Deathrun;
import com.thecreepercow.deathrun.Lang;
import com.thecreepercow.deathrun.Setup.ITrap;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Dispenser;
import org.bukkit.entity.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class DispenserTrap implements ITrap {
    private List<Location> dispensers = new ArrayList<>();
    private int cooldown = 15;
    private int fuseTime = 40; //In Ticks
    public DispenserTrap(Location... locations) {
        dispensers.addAll(Arrays.asList(locations));
        initialize();
    }

    public DispenserTrap(List<Location> locations) {
        this.dispensers = locations;
        initialize();
    }

    private void initialize(){
        for (Location dispenser : dispensers) {
            Block block = dispenser.getBlock();
            if (!block.getType().equals(Material.DISPENSER)){
                dispensers.remove(dispenser);
            }
        }
    }
    @Override
    public void trigger() {
        dispensers.forEach(location -> {
            Block block = location.getBlock();
            if (block.getState() instanceof Dispenser){
                Dispenser dispenser = (Dispenser) block.getState();
                List<ItemStack> tstack = Arrays.asList(dispenser.getInventory().getStorageContents());
                tstack.removeIf(itemStack -> itemStack.getType().isAir());
                ItemStack item = tstack.get(0);
                org.bukkit.block.data.type.Dispenser tdis = (org.bukkit.block.data.type.Dispenser) dispenser.getBlockData();
                BlockFace face = tdis.getFacing();
                switch (item.getType()){
                    case ARROW:
                        Projectile proj = dispenser.getBlockProjectileSource().launchProjectile(Arrow.class);
                        proj.setGravity(false);
                        break;
                    case FIRE_CHARGE:
                        fireSnake(location, face);
                        break;
                    case TNT:
                        Location loc = block.getLocation().clone();
                        loc.add(face.getModX(),face.getModY(),face.getModZ());
                        loc.getWorld().spawn(loc,TNTPrimed.class, tntPrimed -> {
                            tntPrimed.setFuseTicks(fuseTime);
                            tntPrimed.setMetadata("dtr_tnt", new FixedMetadataValue(Deathrun.getInstance(), 1));
                        });
                    case ANVIL:
                        Location locanvil = block.getLocation().clone();
                        locanvil.add(face.getModX(),face.getModY(),face.getModZ());
                        locanvil.getWorld().spawnFallingBlock(locanvil,Material.ANVIL.createBlockData()).setMetadata("DTR_fb",new FixedMetadataValue(Deathrun.getInstance(),true));
                        break;
                    default:

                        break;
                }
            }
        });
    }

    @Override
    public int getCooldown() {
        return cooldown;
    }

    public void setCooldown(int cooldown){
        this.cooldown = cooldown;
    }

    public int getFuseTime() {
        return fuseTime;
    }

    public void setFuseTime(int fuseTime) {
        this.fuseTime = fuseTime;
    }

    public void addDispenser(Location location){
        dispensers.add(location);
    }

    private static void fireSnake(@NotNull Location location, @NotNull BlockFace direction){
        World world = location.getWorld();
        Block block = location.getBlock();
        while (block.getType().isAir()){
            world.spawnParticle(Particle.FLAME,block.getLocation(),10);
            Collection<Entity> players = world.getNearbyEntities(location,3,3,3, entity -> entity instanceof Player);
            players.forEach(entity -> {
                Player player = (Player) entity;
                player.damage(40);
            });
            block = block.getRelative(direction);
        }
    }

    @Override
    public String getTrapType() {
        return Lang.getString("DispenserTrapName");
    }
}
