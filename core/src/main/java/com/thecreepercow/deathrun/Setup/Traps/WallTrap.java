package com.thecreepercow.deathrun.Setup.Traps;

import com.thecreepercow.deathrun.Deathrun;
import com.thecreepercow.deathrun.Lang;
import com.thecreepercow.deathrun.Setup.ITrap;
import com.thecreepercow.deathrun.Util.LocationUtil;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.scheduler.BukkitRunnable;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public class WallTrap implements ITrap {
    Location loc1;
    Location loc2;
    int cooldown = 20;
    int delay = 15;
    private transient List<Block> blocks;

    public WallTrap(@NotNull Location loc1, @NotNull Location loc2) {
        this.loc1 = loc1;
        this.loc2 = loc2;
        blocks = LocationUtil.getArea(loc1,loc2);
    }

    @Override
    public void trigger() {
        blocks.forEach(block -> {
            block.setType(Material.valueOf(Deathrun.getInstance().getConfig().getString("Game.wallmaterial")));
        });
        new BukkitRunnable(){
            @Override
            public void run() {
                blocks.forEach(block -> block.setType(Material.AIR));
            }
        }.runTaskLater(Deathrun.getInstance(),delay*20);
    }

    @Override
    public int getCooldown() {
        return cooldown;
    }

    public void setCooldown(int cooldown) {
        this.cooldown = cooldown;
    }

    public int getDelay() {
        return delay;
    }

    public void setDelay(int delay) {
        this.delay = delay;
    }

    @Override
    public String getTrapType() {
        return Lang.getString("WallTrapName");
    }
}
