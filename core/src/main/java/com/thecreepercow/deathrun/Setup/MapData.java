package com.thecreepercow.deathrun.Setup;

import com.thecreepercow.deathrun.Setup.Interactables.Interactable;
import com.thecreepercow.deathrun.Setup.Traps.TrapSign;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.HashMap;

public class MapData {
    private String name;
    private HashMap<String, ITrap> traps = new HashMap<>();
    private HashMap<Location, Interactable> interactables = new HashMap<>();
    private ArrayList<TrapSign> signs = new ArrayList<>();
    private ArrayList<Location> spawns = new ArrayList<>();
    private ArrayList<Location> deathSpawns = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public HashMap<String, ITrap> getTraps() {
        return traps;
    }

    public void setTraps(HashMap<String, ITrap> traps) {
        this.traps = traps;
    }

    public void addTrap(String trapName, ITrap trap){
        this.traps.put(trapName,trap);
    }

    public HashMap<Location, Interactable> getInteractables() {
        return interactables;
    }

    public void setInteractables(HashMap<Location, Interactable> interactables) {
        this.interactables = interactables;
    }

    public void addInteractable(Location loc, Interactable interactable){
        this.interactables.put(loc,interactable);
    }
    public void addSign(TrapSign sign){
        this.signs.add(sign);
    }

    public ArrayList<TrapSign> getSigns() {
        return signs;
    }

    public ArrayList<Location> getSpawns() {
        return spawns;
    }

    public void setSpawns(ArrayList<Location> spawns) {
        this.spawns = spawns;
    }

    public ArrayList<Location> getDeathSpawns() {
        return deathSpawns;
    }

    public void setDeathSpawns(ArrayList<Location> deathSpawns) {
        this.deathSpawns = deathSpawns;
    }

    @Nullable
    public World getWorld(){
        return Bukkit.getWorld(name);
    }
}
