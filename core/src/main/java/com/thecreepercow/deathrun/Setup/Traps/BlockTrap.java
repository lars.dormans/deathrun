package com.thecreepercow.deathrun.Setup.Traps;

import com.thecreepercow.deathrun.Deathrun;
import com.thecreepercow.deathrun.Lang;
import com.thecreepercow.deathrun.Setup.ITrap;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BlockTrap implements ITrap {
    List<Location> blocks = new ArrayList<>();
    int cooldown = 20;
    int delay = 10;
    public BlockTrap(Location... locations) {
        blocks.addAll(Arrays.asList(locations));
    }

    public BlockTrap(List<Location> blocks) {
        this.blocks = blocks;
    }

    @Override
    public void trigger() {
        List<BlockState> orginStates = new ArrayList<>();
        blocks.forEach(location -> {
            Block block = location.getBlock();
            orginStates.add(block.getState());
            block.setType(Material.AIR);

        });
        new BukkitRunnable(){
            @Override
            public void run() {
                orginStates.forEach(blockState -> blockState.update(true));
            }
        }.runTaskLater(Deathrun.getInstance(), delay*20);
    }

    @Override
    public int getCooldown() {
        return cooldown;
    }

    public int getDelay() {
        return delay;
    }

    public void setDelay(int delay) {
        this.delay = delay;
    }

    public void setCooldown(int cooldown) {
        this.cooldown = cooldown;
    }

    @Override
    public String getTrapType() {
        return Lang.getString("BlockTrapName");
    }
}
