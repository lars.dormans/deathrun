package com.thecreepercow.deathrun.Setup.Interactables;

import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class PotionInteractable implements Interactable {
    PotionEffect potion;

    public PotionInteractable(String potion) {
        String[] potionData = potion.split(":");
        PotionEffectType type = PotionEffectType.getByName(potionData[0]);
        if (type==null){
            return;
        }
        this.potion = new PotionEffect(type, Integer.parseInt(potionData[1]), Integer.parseInt(potionData[2]),false,false);
    }

    @Override
    public void activate(Player player) {
        player.addPotionEffect(potion);
    }
}
