package com.thecreepercow.deathrun.Setup;

import com.thecreepercow.deathrun.Deathrun;
import com.thecreepercow.deathrun.Module;

public class SetupModule extends Module {
    private MapManager mapManager;
    private SetupManager setupManager;
    @Override
    public void initialize() {
        mapManager = new MapManager();
        mapManager.initialize();
        if (Deathrun.getInstance().isSetupMode()) {
            setupManager = new SetupManager();
            setupManager.initialize();
        }
    }

    public MapManager getMapManager() {
        return mapManager;
    }

    public SetupManager getSetupManager() {
        return setupManager;
    }
}
