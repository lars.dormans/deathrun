package com.thecreepercow.deathrun.Setup;

public interface ITrap {
    void trigger();
    int getCooldown();
    String getTrapType();
}
