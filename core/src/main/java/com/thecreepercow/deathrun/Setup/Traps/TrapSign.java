package com.thecreepercow.deathrun.Setup.Traps;

import com.thecreepercow.deathrun.Deathrun;
import com.thecreepercow.deathrun.Setup.ITrap;
import com.thecreepercow.deathrun.Setup.MapData;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.block.data.type.WallSign;
import org.bukkit.scheduler.BukkitRunnable;

public class TrapSign {
    private String trap;
    private Location sign;
    boolean active = true;
    private transient ITrap itrap;
    private transient Sign psign;

    public TrapSign(String trap, Location sign) {
        this.trap = trap;
        this.sign = sign;
    }

    public String getTrap() {
        return trap;
    }

    public Location getSign() {
        return sign;
    }

    public boolean isActive() {
        return active;
    }

    public void initalizeSign(MapData data){
        Block block = sign.getBlock();
        if (block.getState() instanceof Sign){
            itrap = data.getTraps().get(trap);
            Sign sign = (Sign) block.getState();
            psign = sign;
            sign.setLine(0, ChatColor.DARK_PURPLE+"[DTR]");
            sign.setLine(1, ChatColor.DARK_AQUA+itrap.getTrapType());
            sign.setLine(2, ChatColor.GOLD+"0");
            sign.update();
            WallSign signData = (org.bukkit.block.data.type.WallSign) block.getState().getData();
            Block behindSign = block.getRelative(signData.getFacing().getOppositeFace());
            behindSign.setType(Material.LIME_CONCRETE);
        }
    }
    public void afterTrigger(){
        active = false;
        Block block = sign.getBlock();
        WallSign signData = (org.bukkit.block.data.type.WallSign) block.getState().getData();
        Block behindSign = block.getRelative(signData.getFacing().getOppositeFace());
        behindSign.setType(Material.RED_CONCRETE);
        psign.setLine(2, ChatColor.GOLD+String.valueOf(itrap.getCooldown()));
        new BukkitRunnable(){
            int cooldown = itrap.getCooldown();
            Sign sign = psign;
            @Override
            public void run() {
                cooldown-=1;
                sign.setLine(2, ChatColor.GOLD+String.valueOf(cooldown));
                sign.update();
                if (cooldown==0){
                    afterCooldown();
                    cancel();
                }
            }
        }.runTaskTimer(Deathrun.getInstance(), 0, 20);
    }
    public void afterCooldown(){
        Block block = sign.getBlock();
        WallSign signData = (org.bukkit.block.data.type.WallSign) block.getState().getData();
        Block behindSign = block.getRelative(signData.getFacing().getOppositeFace());
        behindSign.setType(Material.LIME_CONCRETE);
        active = true;
    }
}
