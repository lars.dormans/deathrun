package com.thecreepercow.deathrun.Setup.Traps;

import com.thecreepercow.deathrun.Deathrun;
import com.thecreepercow.deathrun.Setup.ITrap;
import com.thecreepercow.deathrun.Util.LocationUtil;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.data.BlockData;
import org.bukkit.entity.FallingBlock;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.scheduler.BukkitRunnable;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class BridgeTrap implements ITrap {
    private Location loc1;
    private Location loc2;
    private int cooldown = 10;
    private int delay = 10;
    private transient List<Block> blocks;
    public BridgeTrap(@NotNull Location loc1, @NotNull Location loc2) {
        this.loc1 = loc1;
        this.loc2 = loc2;
        blocks = LocationUtil.getArea(loc1,loc2);
    }

    @Override
    public void trigger() {
        List<BlockState> orginState = new ArrayList<>();
        blocks.forEach(block -> {
            BlockState oState = block.getState();
            orginState.add(oState);
            BlockData data = block.getBlockData();
            block.setType(Material.AIR);
            FallingBlock benetity = block.getWorld().spawnFallingBlock(block.getLocation(),data);
            benetity.setMetadata("DTR_fb", new FixedMetadataValue(Deathrun.getInstance(),true));
        });
        new BukkitRunnable(){
            @Override
            public void run() {
                orginState.forEach(blockState -> blockState.update(true));
            }
        }.runTaskLater(Deathrun.getInstance(), delay*20);
    }

    @Override
    public int getCooldown() {
        return cooldown;
    }
    public void setCooldown(int cooldown){
        this.cooldown = cooldown;
    }

    public int getDelay() {
        return delay;
    }

    public void setDelay(int delay) {
        this.delay = delay;
    }

    @Override
    public String getTrapType() {
        return null;
    }
}
