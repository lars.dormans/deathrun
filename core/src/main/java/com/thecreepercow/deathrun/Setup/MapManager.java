package com.thecreepercow.deathrun.Setup;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.thecreepercow.deathrun.Deathrun;
import com.thecreepercow.deathrun.Lang;
import com.thecreepercow.deathrun.Module;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.util.FileUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.*;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Set;

public class MapManager extends Module {
    private MapData activeMap;
    private File mapPath;
    @NotNull
    private HashMap<String,MapData> maps = new HashMap<>();
    @NotNull
    private Gson gson = new GsonBuilder().enableComplexMapKeySerialization().excludeFieldsWithModifiers(Modifier.TRANSIENT).serializeNulls().create();


    @Override
    public void initialize() {
        mapPath = new File(Deathrun.getInstance().getDataFolder() + File.separator + "maps");
        File[] dirs = mapPath.listFiles(File::isDirectory);
        for (File dir : dirs) {
            MapData data = loadMap(dir);
            maps.put(data.getName(), data);
            ;
        }
    }

    public void setupMap(@NotNull String worldName, @NotNull Player executor) {
        if (!mapPath.exists()) {
            mapPath.mkdirs();
        }
        World world = Bukkit.getWorld(worldName);
        if (world != null) {
            world.getPlayers().forEach(player -> {
                player.sendMessage(Lang.getString("SetupClearingWorld"));
                if (Deathrun.getInstance().isBungeeMode()){
                    String server = Deathrun.getInstance().getConfig().getString("Bungee.lobby-server");
                    ByteArrayDataOutput out = ByteStreams.newDataOutput();
                    out.writeUTF("Connect");
                    out.writeUTF(server);
                    player.sendPluginMessage(Deathrun.getInstance(),"BungeeCord",out.toByteArray());
                }else {
                    String lobbyworld = Deathrun.getInstance().getConfig().getString("lobbyWorld");
                    Location tp = Bukkit.getWorld(lobbyworld).getSpawnLocation();
                    player.teleport(tp, PlayerTeleportEvent.TeleportCause.PLUGIN);
                }
            });
            Bukkit.unloadWorld(world,true);
            File source = world.getWorldFolder();
            File dest = new File(mapPath + File.separator + worldName);
            FileUtil.copy(source, dest);
            MapData mapData = new MapData();
            mapData.setName(worldName);
            saveMap(worldName,mapData);
            setActiveMap(worldName);
            executor.teleport(world.getSpawnLocation());
        }else{
            executor.sendMessage(Lang.getString("SetupInvalidWorld"));
        }
    }
    public void setActiveMap(String mapName){
        if (maps.containsKey(mapName)){
            activeMap = maps.get(mapName);
            File source = new File(mapPath + File.separator + mapName);
            File dest = new File(Bukkit.getWorldContainer() + mapName);
            FileUtil.copy(source,dest);
            Bukkit.createWorld(new WorldCreator(mapName));
        }
    }
    public void saveMap(String mapName, MapData data){
        File mapDataFile = new File(mapPath+File.separator+mapName+File.separator+"data.json");
        if (!mapDataFile.exists()){
            try {
                mapDataFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try(Writer writer = new FileWriter(mapDataFile)) {
            gson.toJson(data,writer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @Nullable
    private MapData loadMap(String mapName){
        File dataFile = new File(mapPath+File.separator+mapName+File.separator+"data.json");
        try {
            JsonReader reader = new JsonReader(new FileReader(dataFile));
            return gson.fromJson(reader, new TypeToken<MapData>(){}.getType());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
    private MapData loadMap(File map){
        try {
            JsonReader reader = new JsonReader(new FileReader(map));
            return gson.fromJson(reader, new TypeToken<MapData>(){}.getType());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public MapData getActiveMap() {
        return activeMap;
    }
    public MapData getMap(String mapName){
         return maps.get(mapName);
    }
    public Set<String> getMaps(){
        return maps.keySet();
    }
}
