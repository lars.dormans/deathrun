package com.thecreepercow.deathrun.Setup.Interactables;

import org.bukkit.Location;
import org.bukkit.entity.Player;

public class TeleportInteractable implements Interactable {
    Location telLoc;

    public TeleportInteractable(Location telLoc) {
        this.telLoc = telLoc;
    }

    @Override
    public void activate(Player player) {
        player.teleport(telLoc);
    }
}
