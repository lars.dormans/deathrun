package com.thecreepercow.deathrun.Setup.Interactables;

import org.bukkit.entity.Player;

public interface Interactable {
    void activate(Player player);
}
